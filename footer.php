        <footer>
            <div class="widgets">
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer") ) { ?>
            &nbsp;
            <?php } ?>
            </div>
            <nav>
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
            </nav>
            <div class="credits">
                <p>Copyright <?php echo date('Y')." "; bloginfo('name'); ?></p>
            </div>
        </footer>
        </div>
<?php wp_footer(); ?>
</body>
</html>
