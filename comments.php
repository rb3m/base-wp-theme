<?php if (comments_open()) { ?>
	<div id="comments">
	<h2>Comments</h2>
	<?php if ( have_comments() ) : ?>
		<h2 id="comments-title">
			<?php comments_number('No comments', 'One comment', '% comments'); ?>
		</h2>
		<ol class="commentlist">
			<?php
				wp_list_comments();
			?>
		</ol>

		<?php if ( get_comment_pages_count() > 1) : ?>
		<nav id="comment-nav-below">
			<div class="nav-previous"><?php previous_comments_link('Next comments'); ?></div>
			<div class="nav-next"><?php next_comments_link('Previous comments'); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>
	<?php endif; ?>
	<?php comment_form(); ?>
	</div>
<?php } ?>