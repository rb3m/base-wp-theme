        <header>
            <hgroup>
                    <p id="site-title"><span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span></h1>
                    <p id="site-description"><?php bloginfo( 'description' ); ?></h2>
            </hgroup>
            <nav id="mainNav" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?> 
            </nav>
        </header>