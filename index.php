<?php get_header(); ?>
        <section id="latestPost">
        <?php
        $firstPost = TRUE;
	while ( have_posts() ) : the_post(); ?>
                <article>
                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    <p class="date"><?php $fecha = get_the_date('d-m-Y'); echo $fecha; ?></p>
                    <?php ($firstPost) ? the_content() : the_excerpt(); ?>
		    <?php if (comments_open() && ("post" == get_post_type())) { ?>
                    <p class="comments">Comments: <?php comments_number ('0','1','%'); ?></p>
		    <?php } ?>
                </article>
            <?php if ($firstPost) { ?>
                </section>
                <?php
                include('siteNavigation.php');
                $firstPost = FALSE;
                ?>                
                <section id="previousPosts">
            <?php } 
        endwhile; ?>            
        </section>
        <nav class="pagesNav">
            <p class="nextPost"><?php previous_posts_link( '<span class="meta-nav">&larr;</span> Previous Page' ); ?></p>
            <p class="previousPost"><?php next_posts_link( 'Next Page <span class="meta-nav">&rarr;</span>' ); ?></p>
        </nav>        
<?php
        get_sidebar();
        get_footer();
?>