<?php
add_theme_support('menus');
add_theme_support('post-thumbnails');
add_action( 'init', 'register_menus' );
if (!is_admin()) add_action("wp_enqueue_scripts", "jquery_enqueue", 11);

function jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http://code.jquery.com/jquery-1.10.2.min.js", false, null);
   wp_enqueue_script('jquery');
}

function register_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}

if ( function_exists('register_sidebar') ) {
   register_sidebar(
        array (
               'name' => 'Footer',
               'before_widget' => '<div class="widget">',
               'after_widget' => '</div>',
               'before_title' => '<h2>',
               'after_title' => '</h2>'
               )
        );
    register_sidebar(
        array (
               'name' => 'Widgets1',
               'before_widget' => '<div class="widget">',
               'after_widget' => '</div>',
               'before_title' => '<h2>',
               'after_title' => '</h2>'
               )
        );
    register_sidebar(
        array (
               'name' => 'Widgets2',
               'before_widget' => '<div class="widget">',
               'after_widget' => '</div>',
               'before_title' => '<h2>',
               'after_title' => '</h2>'
               )
        );
}
