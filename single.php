<?php get_header(); ?>
        <section id="latestPost">
        <?php
	while ( have_posts() ) : the_post(); ?>
                <article>
                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    <p class="date"><?php $fecha = get_the_date('d-m-Y'); echo $fecha; ?></p>
		    <div class="content">
                    <?php the_content(); ?>
		    </div>
                    <nav class="postNav">
                        <p class="nextPost"><?php previous_post_link(); ?></p>
                        <p class="previousPost"><?php next_post_link(); ?></p>
                    </nav>
                    <?php
                    comments_template();
                    ?>
                </article>
            <?php 
        endwhile; ?>
        </section>
<?php include('siteNavigation.php');?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>